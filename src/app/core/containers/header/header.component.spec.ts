import { HeaderComponent } from './header.component';
import { ComponentFixture , TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { User } from '../../models/user.model';
import { BehaviorSubject } from 'rxjs';
import { AuthenticationService } from '../../services/authentication.service';
import { RouterTestingModule } from '@angular/router/testing';

const testUser: User = {
    id: 1,
    username: 'User 1',
    token: 'randomWebToken',
    admin: 1,
};

const userSubject = new BehaviorSubject<User>(testUser);

class MockService {
    currentUser = userSubject.asObservable();
    logout() {}
}

describe('HeaderComponent', () => {
    let component: HeaderComponent;
    let fixture: ComponentFixture<HeaderComponent>;
    let service: MockService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                HeaderComponent,
            ],
            imports: [
                RouterTestingModule,
                HttpClientTestingModule,
            ],
            providers: [{ provide: AuthenticationService, useClass: MockService }],
        });
        fixture = TestBed.createComponent(HeaderComponent);
        component = fixture.componentInstance;
        service = TestBed.get(AuthenticationService);
    });
    describe('OnInit', () => {
        it('should set the user', () => {
            component.ngOnInit();
            expect(component.currentUser).toEqual(testUser);
        });
    });
    describe('Logout', () => {
        it('should logout the user', () => {
            const spy = spyOn(service, 'logout');
            component.logout();
            expect(spy).toHaveBeenCalledTimes(1);
        });
    });
});
