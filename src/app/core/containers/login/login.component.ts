import { Component , OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { FormBuilder , FormGroup } from '@angular/forms';
import { ActivatedRoute , Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    returnUrl: string;

    constructor(
        private authenticationService: AuthenticationService,
        private fb: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
    ) {
        if (this.authenticationService.currentUserValue) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit(): void {
        this.buildLoginForm();

        this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
    }

    buildLoginForm() {
        this.loginForm = this.fb.group({
            username: this.fb.control([]),
            password: this.fb.control([]),
        });
    }

    login() {
        this.authenticationService.login(this.loginForm.get('username').value, this.loginForm.get('password').value)
            .subscribe(data => {
                this.router.navigate([this.returnUrl]);
            });
    }
}
