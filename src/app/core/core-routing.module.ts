import { RouterModule , Routes } from '@angular/router';
import { HomepageComponent } from './containers/homepage/homepage.component';
import { NgModule } from '@angular/core';
import { LoginComponent } from './containers/login/login.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: HomepageComponent,
    },
    {
        path: 'login',
        component: LoginComponent,
    },
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CoreRoutingModule {}
