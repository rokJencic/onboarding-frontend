import { Component , EventEmitter , OnInit , Output } from '@angular/core';
import { FormBuilder , FormGroup } from '@angular/forms';
import { Location } from '../../../models/event.model';

@Component({
    selector: 'app-add-location',
    templateUrl: './add-location.component.html',
    styleUrls: ['./add-location.component.scss']
})
export class AddLocationComponent implements OnInit {
    addLocationForm: FormGroup;
    @Output() location = new EventEmitter<Location>();

    constructor(private fb: FormBuilder) {}

    ngOnInit(): void {
        this.buildLocationForm();
    }

    buildLocationForm() {
        this.addLocationForm = this.fb.group({
            name: this.fb.control([]),
            country: this.fb.control([]),
        });
    }

    submitLocationForm() {
        this.location.emit(this.addLocationForm.value);
    }
}
