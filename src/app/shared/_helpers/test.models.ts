import { Company, Location, Event } from '../../models/event.model';

export const testLocations: Location[] = [
    {
        id: 1,
        name: 'Test Location 1',
        country: 'Test Country 1',
    },
    {
        id: 2,
        name: 'Test Country 2',
        country: 'Test Country 2',
    },
];

export const testCompanies: Company[] = [
    {
        id: 1,
        name: 'Test Company 1',
        location: testLocations[0],
    },
    {
        id: 2,
        name: 'Test Company 2',
        location: testLocations[1],
    },
];

export const testEvents: Event[] = [
    {
        id: 1,
        name: 'Test Event 1',
        company: testCompanies[0],
        location: testLocations[0],
    },
    {
        id: 2,
        name: 'Test Event 2',
        company: testCompanies[1],
        location: testLocations[1],
    },
];
