import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { AddEvent } from '../../models/event.model';

@Injectable({
    providedIn: 'root',
})
export class EventsService {

    constructor(public http: HttpClient) {}

    getEvents(page: number = 1): Observable<any> {
        return this.http.get(`${environment.apiBaseUrl}/events?page=${page}`);
    }

    addEvent(event: AddEvent): Observable<any> {
        return this.http.post(`${environment.apiBaseUrl}/events`, event);
    }

    getEvent(id: number): Observable<any> {
        return this.http.get(`${environment.apiBaseUrl}/events/${id}`);
    }

    updateEvent(id: number, event: { name: string, company_id: number, location_id: number }) {
        return this.http.put(`${environment.apiBaseUrl}/events/${id}`, event);
    }

    deleteEvent(id: number) {
        return this.http.delete(`${environment.apiBaseUrl}/events/${id}`);
    }
}
