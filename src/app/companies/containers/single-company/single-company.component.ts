import { Component , OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CompaniesService } from '../../services/companies.service';
import { Company , Location } from '../../../models/event.model';
import { LocationsService } from '../../../locations/services/locations.service';

@Component({
    selector: 'app-single-company',
    templateUrl: './single-company.component.html',
    styleUrls: ['./single-company.component.scss'],
})
export class SingleCompanyComponent implements OnInit {
    company: Company;
    locations: Location[];
    unsavedChanges = false;

    constructor(
        private route: ActivatedRoute,
        private companiesService: CompaniesService,
        private locationsService: LocationsService,
    ) {}

    ngOnInit(): void {
        this.loadCompany();
        this.loadLocations();
    }

    loadCompany() {
        const id = +this.route.snapshot.paramMap.get('id');
        this.companiesService.getCompany(id).subscribe(company => this.company = company);
    }

    loadLocations() {
        this.locationsService.getLocations().subscribe(locations => this.locations = locations.data.locations);
    }

    updateCompany(value) {
        this.company = {
            ...this.company,
            [value.name]: value.data,
        };
    }

    saveChanges() {
        this.companiesService.updateCompany(this.company.id, {name: this.company.name, location_id: this.company.location.id}).subscribe();
    }

    deleteCompany() {
        this.companiesService.deleteCompany(this.company.id).subscribe();
    }

    updateLocation(location) {
        this.company.location = location;
    }
}
