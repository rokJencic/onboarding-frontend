import { TestBed } from '@angular/core/testing';
import { CompaniesService } from './companies.service';
import { of } from 'rxjs';
import { Company } from '../../models/event.model';
import { testCompanies } from '../../shared/_helpers/test.models';
import { HttpClientTestingModule } from '@angular/common/http/testing';

const companies: Company[] = testCompanies;

describe('CompaniesService', () => {
    let service: CompaniesService;
    let httpClientSpy: {
        get: jasmine.Spy,
        post: jasmine.Spy,
        put: jasmine.Spy,
        delete: jasmine.Spy,
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [CompaniesService],
        });
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
        service = new CompaniesService(httpClientSpy as any);
    });
    describe('GetCompany', () => {
        it('should return the company', () => {
            httpClientSpy.get.and.returnValue(of(testCompanies[0]));
            service.getCompany(1).subscribe(response => {
                expect(response).toEqual(testCompanies[0]);
            });
        });
    });
    describe('getCompanies', () => {
        it('should retrieve the companies', () => {
            httpClientSpy.get.and.returnValue(of(companies));
            service.getCompanies().subscribe(response => {
                    expect(response).toEqual(companies);
                },
                fail
            );
            expect(httpClientSpy.get.calls.count()).toBe(1);
        });
    });

    describe('addCompany', () => {
        it('should post the company', () => {
            httpClientSpy.post.and.returnValue(of(companies[0]));
            service.addCompany({ name: 'Test Company', location_id: 1}).subscribe(response => {
                    expect(response).toEqual(companies[0]);
                },
                fail
            );
            expect(httpClientSpy.post.calls.count()).toBe(1);
        });
    });

    describe('updateCompany', () => {
        it('should update the location', () => {
            httpClientSpy.put.and.returnValue(of(companies[0]));
            service.updateCompany(companies[0].id, { name: 'Test Company', location_id: 1 }).subscribe(response => {
                    expect(response).toEqual(companies[0]);
                },
                fail
            );
            expect(httpClientSpy.put.calls.count()).toBe(1);
        });
    });

    describe('deleteCompany', () => {
        it('should delete the location', () => {
            httpClientSpy.delete.and.returnValue(of(companies[0].id));
            service.deleteCompany(companies[0].id).subscribe(response => {
                    expect(response).toEqual(companies[0].id);
                },
                fail
            );
            expect(httpClientSpy.delete.calls.count()).toBe(1);
        });
    });
});
