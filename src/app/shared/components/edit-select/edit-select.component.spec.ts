import { EditSelectComponent , Option } from './edit-select.component';
import { ComponentFixture , TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

const testOptions: Option[] = [
    { id: 1, name: 'Name' },
    { id: 2, name: 'Option 2' },
];

describe('EditSelectComponent', () => {
    let component: EditSelectComponent;
    let fixture: ComponentFixture<EditSelectComponent>;
    let el: DebugElement;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [EditSelectComponent],
            imports: [FormsModule],
        });
        fixture = TestBed.createComponent(EditSelectComponent);
        component = fixture.componentInstance;
        el = fixture.debugElement;

        component.data = testOptions[0];
        component.name = 'Name';
        component.options = testOptions;
    });

    describe('Set properties', () => {
        it('should set all input properties', () => {
            expect(component.options).toEqual(testOptions);
            expect(component.selectedOption).toEqual(testOptions[0]);
            expect(component.editMode).toBeFalsy();
            expect(component.name).toEqual('Name');
            expect(component.data).toEqual(testOptions[0]);
        });
    });

    describe('onFocusOut', () => {
        it('should emit correct event', () => {
            spyOn(component.focusOut, 'emit');
            component.onFocusOut();
            expect(component.focusOut.emit).toHaveBeenCalledTimes(1);
            expect(component.focusOut.emit).toHaveBeenCalledWith(testOptions[0]);
        });
    });
    describe('ComponentTemplate', () => {
        it('should render the correctly', () => {
            fixture.detectChanges();
            const label = el.query(By.css('.form-group label'));
            expect(label.nativeElement.textContent).toEqual('NAME: ');
        });

        it('should render the select on editMode change', () => {
            component.editMode = true;
            fixture.detectChanges();

            const label = el.query(By.css('.form-group label')).nativeElement;
            expect(label.textContent).toEqual('NAME: ');
            const select = el.query(By.css('.form-group select.form-control')).nativeElement;
            expect(select.getAttribute('id')).toBe('Name');
        });
    });
});
