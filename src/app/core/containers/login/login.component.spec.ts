import { LoginComponent } from './login.component';
import { ComponentFixture , TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { ActivatedRoute , Router } from '@angular/router';
import { User } from '../../models/user.model';
import { BehaviorSubject , of } from 'rxjs';
import { AuthenticationService } from '../../services/authentication.service';
import { FormBuilder , ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';

const testUser: User = {
    id: 1,
    username: 'User 1',
    token: 'randomWebToken',
    admin: 1,
};

const userSubject = new BehaviorSubject<User>(testUser);

class MockService {
    currentUser = userSubject.asObservable();
    currentUserValue = testUser;
    login(username, password) {
        return of({});
    }
}

describe('LoginComponent', () => {
    let component: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    let el: DebugElement;
    let route = {
        snapshot: {
            queryParams: {
                returnUrl: 'returnUrl',
            },
        },
    };
    let service: MockService;
    let router = {
        navigate: jasmine.createSpy('navigate'),
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                LoginComponent,
            ],
            providers: [
                { provide: ActivatedRoute, useValue: route },
                { provide: AuthenticationService, useClass: MockService },
                { provide: Router, useValue: router },
                FormBuilder,
            ],
            imports: [
                ReactiveFormsModule,
                RouterTestingModule,
            ],
        });
        fixture = TestBed.createComponent(LoginComponent);
        component = fixture.componentInstance;
        el = fixture.debugElement;
        route = TestBed.get(ActivatedRoute);
        service = TestBed.get(AuthenticationService);
        router = TestBed.get(Router);
    });
    describe('OnInit', () => {
        it('should navigate away if user exists', () => {
            expect(router.navigate).toHaveBeenCalledWith(['/']);
        });
        it('should set the component', () => {
            component.ngOnInit();
            expect(component.returnUrl).toEqual('returnUrl');
        });
    });
    describe('BuildLoginForm', () => {
        it('should build the form', () => {
            component.buildLoginForm();
            fixture.detectChanges();
            const inputs = el.queryAll(By.css('.form-group input'));
            expect(inputs.length).toEqual(2);
            const button = el.query(By.css('.form-group button'));
            expect(button.nativeElement.textContent).toBe('Login');
        });
    });
    describe('Login', () => {
        it('should log the user in and redirect', () => {
            const spy = spyOn(service, 'login').and.callThrough();
            component.ngOnInit();
            component.loginForm.setValue({ username: 'username', password: 'password' });
            component.login();
            expect(spy).toHaveBeenCalledWith('username', 'password');
            expect(router.navigate).toHaveBeenCalledWith(['returnUrl']);
        });
    });
});
