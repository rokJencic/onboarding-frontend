import { Component , OnInit } from '@angular/core';
import { CompaniesService } from '../../services/companies.service';
import { AddCompany , Company , Location } from '../../../models/event.model';
import { LocationsService } from '../../../locations/services/locations.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-companies',
    templateUrl: './companies.component.html',
    styleUrls: ['./companies.component.scss'],
})
export class CompaniesComponent implements OnInit {

    companies: Company[];
    locations: Location[];
    addCompanyMode = false;
    currentPage: number;
    lastPage: number;

    constructor(
        private companiesService: CompaniesService,
        private locationsService: LocationsService,
        private router: Router,
    ) {}

    ngOnInit(): void {
        this.getCompanies();
        this.getLocations();
    }

    changeMode() {
        this.addCompanyMode = !this.addCompanyMode;
    }

    getCompanies() {
        this.companiesService.getCompanies().subscribe(response => {
            this.companies = response.data.companies;
            this.currentPage = response.meta.current_page;
            this.lastPage = response.meta.last_page;
        });
    }

    getLocations() {
        this.locationsService.getLocations().subscribe(locations => this.locations = locations.data.locations);
    }

    addCompany(company: AddCompany) {
        this.companiesService.addCompany(company).subscribe();
    }

    onCompanyClick(id: number) {
        this.router.navigate(['/companies', id]);
    }

    onPageClick(page: number) {
        this.companiesService.getCompanies(page).subscribe(response => {
            this.companies = response.data.companies;
            this.currentPage = response.meta.current_page;
        });
    }
}
