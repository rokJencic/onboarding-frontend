import { AddEventComponent } from './add-event.component';
import { ComponentFixture , TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormBuilder , ReactiveFormsModule } from '@angular/forms';
import { testCompanies , testLocations } from '../../../shared/_helpers/test.models';
import { By } from '@angular/platform-browser';

describe('AddEventComponent', () => {
    let component: AddEventComponent;
    let fixture: ComponentFixture<AddEventComponent>;
    let el: DebugElement;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [AddEventComponent],
            imports: [ReactiveFormsModule],
            providers: [FormBuilder],
        });
        fixture = TestBed.createComponent(AddEventComponent);
        component = fixture.componentInstance;
        el = fixture.debugElement;
        component.locations = testLocations;
        component.companies = testCompanies;
    });
    describe('Set properties', () => {
        it('should set the properties', () => {
            expect(component.locations).toEqual(testLocations);
            expect(component.companies).toEqual(testCompanies);
        });
    });
    describe('BuildEventForm', () => {
        it('should build the form', () => {
            component.buildEventForm();
            fixture.detectChanges();
            const inputs = el.queryAll(By.css('.form-group input'));
            expect(inputs.length).toEqual(1);
            const select = el.queryAll(By.css('.form-group select'));
            expect(select.length).toEqual(2);
            const button = el.query(By.css('.form-group button'));
            expect(button.nativeElement.textContent).toBe('Add event');
        });
    });
    describe('SubmitEventForm', () => {
        it('should emit correct event', () => {
            spyOn(component.event, 'emit');
            component.ngOnInit();
            component.addEventForm.setValue({ name: 'Name', location_id: 1, company_id: 1 });
            component.submitEventForm();
            expect(component.event.emit).toHaveBeenCalledTimes(1);
            expect(component.event.emit).toHaveBeenCalledWith({ name: 'Name', location_id: 1, company_id: 1 });
        });
    });
});
