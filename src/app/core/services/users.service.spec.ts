import { User } from '../models/user.model';
import { UsersService } from './users.service';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

const testUsers: User[] = [
    {
        id: 1,
        username: 'User 1',
        token: 'randomWebToken',
        admin: 1,
    },
    {
        id: 2,
        username: 'User 2',
        token: 'randomWebToken',
        admin: 0,
    },
];

describe('UsersService', () => {
    let service: UsersService;
    let httpClientSpy: {
        get: jasmine.Spy,
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [UsersService],
            imports: [HttpClientTestingModule],
        });
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
        service = new UsersService(httpClientSpy as any);
    });
    describe('getAllUsers', () => {
        it('should return all users', () => {
            httpClientSpy.get.and.returnValue(of(testUsers));
            service.getAllUsers().subscribe(value => {
                expect(value).toEqual(testUsers);
                expect(httpClientSpy.get.calls.count()).toBe(1);
            });
        });
    });
    describe('GetUser', () => {
        it('should return the user', () => {
            httpClientSpy.get.and.returnValue(of(testUsers[0]));
            service.getUser(1).subscribe(value => {
                expect(value).toEqual(testUsers[0]);
                expect(httpClientSpy.get.calls.count()).toBe(1);
            });
        });
    });
});
