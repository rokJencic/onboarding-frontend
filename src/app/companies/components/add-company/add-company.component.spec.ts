import { AddCompanyComponent } from './add-company.component';
import { ComponentFixture , TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormBuilder , ReactiveFormsModule } from '@angular/forms';
import { testLocations } from '../../../shared/_helpers/test.models';
import { By } from '@angular/platform-browser';

describe('AddCompanyComponent', () => {
    let component: AddCompanyComponent;
    let fixture: ComponentFixture<AddCompanyComponent>;
    let el: DebugElement;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [AddCompanyComponent],
            providers: [FormBuilder],
            imports: [ReactiveFormsModule],
        });
        fixture = TestBed.createComponent(AddCompanyComponent);
        component = fixture.componentInstance;
        el = fixture.debugElement;
        component.locations = testLocations;
    });
    describe('Set properties', () => {
        it('should set the correct properties', () => {
            expect(component.locations).toEqual(testLocations);
        });
    });
    describe('BuildCompanyForm', () => {
        it('should create the form', () => {
            component.buildCompanyForm();
            fixture.detectChanges();
            const inputs = el.queryAll(By.css('.form-group input'));
            expect(inputs.length).toEqual(1);
            const select = el.queryAll(By.css('.form-group select'));
            expect(select.length).toEqual(1);
            const button = el.query(By.css('.form-group button'));
            expect(button.nativeElement.textContent).toBe('Add company');
        });
    });
    describe('SubmitCompanyForm', () => {
        it('should emit correct event', () => {
            spyOn(component.company, 'emit');
            component.ngOnInit();
            component.addCompanyForm.setValue({ name: 'Name', location_id: 1 });
            component.submitCompanyForm();
            expect(component.company.emit).toHaveBeenCalledTimes(1);
            expect(component.company.emit).toHaveBeenCalledWith({ name: 'Name', location_id: 1 });
        });
    });
});
