import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { EditInputComponent } from './components/edit-input/edit-input.component';
import { AutoFocusDirective } from './directives/auto-focus.directive';
import { EditSelectComponent } from './components/edit-select/edit-select.component';
import { PaginationComponent } from './components/pagination/pagination.component';

@NgModule({
    declarations: [
        EditInputComponent,
        EditSelectComponent,
        PaginationComponent,
        AutoFocusDirective,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        EditInputComponent,
        EditSelectComponent,
        PaginationComponent,
        AutoFocusDirective,
    ],
    providers: []
})
export class SharedModule {}
