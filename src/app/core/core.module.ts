import { NgModule } from '@angular/core';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { HomepageComponent } from './containers/homepage/homepage.component';
import { CoreRoutingModule } from './core-routing.module';
import { HTTP_INTERCEPTORS , HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';
import { JwtInterceptor } from './interceptors/jwt.interceptor';
import { LoginComponent } from './containers/login/login.component';
import { HeaderComponent } from './containers/header/header.component';

@NgModule({
    declarations: [
        HomepageComponent ,
        LoginComponent ,
        HeaderComponent ,
    ] ,
    imports: [
        HttpClientModule ,
        SharedModule ,
        CoreRoutingModule ,
    ] ,
    exports: [
        HeaderComponent
    ] ,
    providers: [
        {provide: HTTP_INTERCEPTORS , useClass: ErrorInterceptor , multi: true} ,
        {provide: HTTP_INTERCEPTORS , useClass: JwtInterceptor , multi: true} ,
    ]
})
export class CoreModule {}
