import { CompaniesComponent } from './companies.component';
import { ComponentFixture , TestBed } from '@angular/core/testing';
import { EMPTY , of } from 'rxjs';
import { testCompanies , testLocations } from '../../../shared/_helpers/test.models';
import { AddCompanyComponent } from '../../components/add-company/add-company.component';
import { PaginationComponent } from '../../../shared/components/pagination/pagination.component';
import { Router } from '@angular/router';
import { CompaniesService } from '../../services/companies.service';
import { LocationsService } from '../../../locations/services/locations.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';

describe('CompaniesComponent', () => {
    let component: CompaniesComponent;
    let fixture: ComponentFixture<CompaniesComponent>;
    let router = {
        navigate: jasmine.createSpy('navigate'),
    };
    const locationsService = jasmine.createSpyObj('LocationsService', {
        getLocations: of({ data: { locations: testLocations }, meta: {}}),
        addLocation: EMPTY,
    });
    const companiesService = jasmine.createSpyObj('CompaniesService', {
        getCompanies: of({ data: { companies: testCompanies }, meta: {}}),
        addCompany: EMPTY,
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                CompaniesComponent,
                AddCompanyComponent,
                PaginationComponent,
            ],
            providers: [
                { provide: Router, useValue: router },
                { provide: CompaniesService, useValue: companiesService },
                { provide: LocationsService, useValue: locationsService },
            ],
            imports: [
                HttpClientTestingModule,
                ReactiveFormsModule,
            ],
        });
        fixture = TestBed.createComponent(CompaniesComponent);
        component = fixture.componentInstance;
        router = TestBed.get(Router);
        companiesService.getCompanies.calls.reset();
        locationsService.getLocations.calls.reset();
    });
    describe('OnInit', () => {
        it('should fire correct methods', () => {
            component.ngOnInit();
            expect(companiesService.getCompanies).toHaveBeenCalledTimes(1);
            expect(locationsService.getLocations).toHaveBeenCalledTimes(1);
        });
    });
    describe('ChangeCompaniesMode', () => {
        it('should change the mode', () => {
            expect(component.addCompanyMode).toBeFalsy();
            component.changeMode();
            expect(component.addCompanyMode).toBeTruthy();
            component.changeMode();
            expect(component.addCompanyMode).toBeFalsy();
        });
    });
    describe('GetCompanies', () => {
        it('should fetch the companies', () => {
            component.getCompanies();
            expect(component.companies).toEqual(testCompanies);
            expect(companiesService.getCompanies).toHaveBeenCalledTimes(1);
        });
    });
    describe('GetLocations', () => {
        it('should fetch the locations', () => {
            component.getLocations();
            expect(locationsService.getLocations).toHaveBeenCalledTimes(1);
            expect(component.locations).toEqual(testLocations);
        });
    });
    describe('AddCompany', () => {
        it('should post the company', () => {
            component.addCompany({ name: 'name', location_id: 1 });
            expect(companiesService.addCompany).toHaveBeenCalledTimes(1);
            expect(companiesService.addCompany).toHaveBeenCalledWith({ name: 'name', location_id: 1 });
        });
    });
    describe('OnCompanyClick', () => {
        it('should navigate to detail page', () => {
            component.onCompanyClick(20);
            expect(router.navigate).toHaveBeenCalledTimes(1);
            expect(router.navigate).toHaveBeenCalledWith(['/companies', 20]);
        });
    });
    describe('OnPageClick', () => {
        it('should fetch the page data', () => {
            component.onPageClick(3);
            expect(companiesService.getCompanies).toHaveBeenCalledTimes(1);
            expect(companiesService.getCompanies).toHaveBeenCalledWith(3);
        });
    });
});
