import { Component , EventEmitter , Input , OnInit , Output } from '@angular/core';
import { FormBuilder , FormGroup } from '@angular/forms';
import { AddEvent , Company , Location } from '../../../models/event.model';

@Component({
    selector: 'app-add-event',
    templateUrl: './add-event.component.html',
    styleUrls: ['./add-event.component.scss'],
})
export class AddEventComponent implements OnInit {
    addEventForm: FormGroup;
    @Input() companies: Company[];
    @Input() locations: Location[];
    @Output() event = new EventEmitter<AddEvent>();

    constructor(private fb: FormBuilder) {}

    ngOnInit(): void {
        this.buildEventForm();
    }

    buildEventForm() {
        this.addEventForm = this.fb.group({
            name: this.fb.control([]),
            company_id: this.fb.control([]),
            location_id: this.fb.control([]),
        });
    }

    submitEventForm() {
        this.event.emit(this.addEventForm.value);
    }
}
