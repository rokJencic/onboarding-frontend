import { PaginationComponent } from './pagination.component';
import { ComponentFixture , TestBed } from '@angular/core/testing';

describe('PaginationComponent', () => {
    let component: PaginationComponent;
    let fixture: ComponentFixture<PaginationComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [PaginationComponent],
        });
        fixture = TestBed.createComponent(PaginationComponent);
        component = fixture.componentInstance;
    });
    describe('Input Properties', () => {
        it('should set the properties correctly', () => {
            component.currentPage = 5;
            component.lastPage = 10;

            expect(component.currentPage).toEqual(5);
            expect(component.lastPage).toEqual(10);
        });
    });
    describe('PageClick', () => {
        it('should emit the correct event', () => {
            spyOn(component.pageClick, 'emit');
            component.onPageClick(7);
            expect(component.pageClick.emit).toHaveBeenCalledTimes(1);
            expect(component.pageClick.emit).toHaveBeenCalledWith(7);
        });
    });
    describe('CreateRange', () => {
        it('should set correct range', () => {
            component.currentPage = 5;
            component.lastPage = 6;
            let result = component.createRange();
            expect(result).toEqual([2, 3, 4, 5, 6]);

            component.currentPage = 2;
            component.lastPage = 6;
            result = component.createRange();
            expect(result).toEqual([1, 2, 3, 4, 5]);
        });
    });
});
