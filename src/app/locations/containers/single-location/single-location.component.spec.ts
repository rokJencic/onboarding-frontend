import { SingleLocationComponent } from './single-location.component';
import { ComponentFixture , TestBed } from '@angular/core/testing';
import { EMPTY , of } from 'rxjs';
import { testLocations } from '../../../shared/_helpers/test.models';
import { LocationsService } from '../../services/locations.service';
import { ActivatedRoute } from '@angular/router';
import { EditInputComponent } from '../../../shared/components/edit-input/edit-input.component';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

describe('SingleLocationComponent', () => {
    let component: SingleLocationComponent;
    let fixture: ComponentFixture<SingleLocationComponent>;
    let route = {
        snapshot: {
            paramMap: {
                get: (key: string) => testLocations[0].id,
            },
        },
    };
    const locationsService = jasmine.createSpyObj('LocationsService', {
        getLocation: of([testLocations[0]]),
        updateLocation: EMPTY,
        deleteLocation: EMPTY,
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                SingleLocationComponent,
                EditInputComponent,
            ],
            providers: [
                { provide: LocationsService, useValue: locationsService },
                { provide: ActivatedRoute, useValue: route }
            ],
            imports: [
                RouterTestingModule,
                FormsModule,
            ],
        });
        fixture = TestBed.createComponent(SingleLocationComponent);
        component = fixture.componentInstance;
        route = TestBed.get(ActivatedRoute);
        locationsService.getLocation.calls.reset();
    });
    describe('OnInit', () => {
        it('should fire correct methods', () => {
            component.ngOnInit();
            expect(locationsService.getLocation).toHaveBeenCalledTimes(1);
        });
    });
    describe('LoadLocations', () => {
        it('should fetch the location', () => {
            component.loadLocation();
            expect(locationsService.getLocation).toHaveBeenCalledTimes(1);
            expect(locationsService.getLocation).toHaveBeenCalledWith(1);
            expect(component.location).toEqual(testLocations[0]);
        });
    });
    describe('UpdateLocation', () => {
        it('should update the location', () => {
            component.location = testLocations[0];
            component.updateLocation({ name: 'name', data: 'New Name' });
            component.updateLocation({ name: 'country', data: 'New Country' });
            expect(component.location).toEqual({ ...testLocations[0], name: 'New Name', country: 'New Country' });
        });
    });
    describe('SaveChanges', () => {
        it('should fire the request', () => {
            const newLocation = { ...testLocations[0], name: 'New Name', country: 'New Country' };
            component.location = newLocation;
            component.saveChanges();
            expect(locationsService.updateLocation).toHaveBeenCalledTimes(1);
            expect(locationsService.updateLocation).toHaveBeenCalledWith(newLocation, newLocation.id);
        });
    });
    describe('DeleteLocation', () => {
        it('should delete the location', () => {
            component.location = testLocations[0];
            component.deleteLocation();
            expect(locationsService.deleteLocation).toHaveBeenCalledTimes(1);
            expect(locationsService.deleteLocation).toHaveBeenCalledWith(testLocations[0].id);
        });
    });
});
