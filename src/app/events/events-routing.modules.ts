import { RouterModule , Routes } from '@angular/router';
import { EventsComponent } from './containers/events/events.component';
import { NgModule } from '@angular/core';
import { SingleEventComponent } from './containers/single-event/single-event.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: EventsComponent
    },
    {
        path: ':id',
        component: SingleEventComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EventsRoutingModule {}
