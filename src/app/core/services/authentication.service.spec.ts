import { AuthenticationService } from './authentication.service';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BehaviorSubject , of } from 'rxjs';
import { User } from '../models/user.model';

const user = {
    id: 1,
    username: 'Username',
    token: 'randomWebToken',
    admin: 1,
};
const userSubject = new BehaviorSubject<User>(user);

describe('AuthenticationService', () => {
    let service: AuthenticationService;
    let httpClientSpy: {
        post: jasmine.Spy,
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [AuthenticationService],
        });
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['post']);
        service = new AuthenticationService(httpClientSpy as any);
    });

    describe('GetCurrentUserValue', () => {
        it('should return the logged in user', () => {
            service.currentUser = userSubject.asObservable();
            service.currentUser.subscribe(value => {
                expect(value).toEqual(user);
            });
        });
    });
    describe('Login', () => {
        it('should login the user', () => {
            httpClientSpy.post.and.returnValue(of(user));
            service.login('username', 'password').subscribe(value => {
                expect(httpClientSpy.post.calls.count()).toBe(1);
            });
            expect(service.currentUserValue).toEqual(user);
        });
    });
    describe('Logout', () => {
        it('should logout the user', () => {
            service.logout();
            expect(service.currentUserValue).toBeNull();
        });
    });
});
