import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { AddCompany } from '../../models/event.model';

@Injectable({
    providedIn: 'root',
})
export class CompaniesService {
    constructor(public http: HttpClient) {}

    getCompanies(page: number = 1): Observable<any> {
        return this.http.get(`${environment.apiBaseUrl}/companies?page=${page}`);
    }

    addCompany(company: AddCompany): Observable<any> {
        return this.http.post(`${environment.apiBaseUrl}/companies`, company);
    }

    getCompany(id: number): Observable<any> {
        return this.http.get(`${environment.apiBaseUrl}/companies/${id}`);
    }

    updateCompany(id: number, company: { name: string, location_id: number }): Observable<any> {
        return this.http.put(`${environment.apiBaseUrl}/companies/${id}`, company);
    }

    deleteCompany(id: number): Observable<any> {
        return this.http.delete(`${environment.apiBaseUrl}/companies/${id}`);
    }
}
