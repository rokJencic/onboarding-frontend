import { EventsComponent } from './events.component';
import { ComponentFixture , TestBed } from '@angular/core/testing';
import { EMPTY , of } from 'rxjs';
import { testCompanies , testEvents , testLocations } from '../../../shared/_helpers/test.models';
import { AddEventComponent } from '../../components/add-event/add-event.component';
import { PaginationComponent } from '../../../shared/components/pagination/pagination.component';
import { Router } from '@angular/router';
import { CompaniesService } from '../../../companies/services/companies.service';
import { LocationsService } from '../../../locations/services/locations.service';
import { EventsService } from '../../services/events.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';

describe('EventsComponent', () => {
    let component: EventsComponent;
    let fixture: ComponentFixture<EventsComponent>;
    let router = {
        navigate: jasmine.createSpy('navigate'),
    };
    const locationsService = jasmine.createSpyObj('LocationsService', {
        getLocations: of({ data: { locations: testLocations }, meta: {}}),
        addLocation: EMPTY,
    });
    const companiesService = jasmine.createSpyObj('CompaniesService', {
        getCompanies: of({ data: { companies: testCompanies }, meta: {}}),
        addCompany: EMPTY,
    });
    const eventsService = jasmine.createSpyObj('EventsService', {
        getEvents: of({ data: { events: testEvents } , meta: {}}),
        addEvent: EMPTY,
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                EventsComponent,
                AddEventComponent,
                PaginationComponent,
            ],
            providers: [
                { provide: Router, useValue: router },
                { provide: CompaniesService, useValue: companiesService },
                { provide: LocationsService, useValue: locationsService },
                { provide: EventsService, useValue: eventsService },
            ],
            imports: [
                HttpClientTestingModule,
                ReactiveFormsModule,
            ],
        });
        fixture = TestBed.createComponent(EventsComponent);
        component = fixture.componentInstance;
        router = TestBed.get(Router);
        eventsService.getEvents.calls.reset();
        companiesService.getCompanies.calls.reset();
        locationsService.getLocations.calls.reset();
    });
    describe('OnInit', () => {
        it('should fire correct methods', () => {
            component.ngOnInit();
            expect(eventsService.getEvents).toHaveBeenCalledTimes(1);
            expect(companiesService.getCompanies).toHaveBeenCalledTimes(1);
            expect(locationsService.getLocations).toHaveBeenCalledTimes(1);
        });
    });
    describe('ChangeEventsMode', () => {
        it('should change the mode', () => {
            expect(component.addEventMode).toBeFalsy();
            component.changeMode();
            expect(component.addEventMode).toBeTruthy();
            component.changeMode();
            expect(component.addEventMode).toBeFalsy();
        });
    });
    describe('GetEvents', () => {
        it('should fetch the events', () => {
            component.getEvents();
            expect(component.events).toEqual(testEvents);
            expect(eventsService.getEvents).toHaveBeenCalledTimes(1);
        });
    });
    describe('GetCompanies', () => {
        it('should fetch the companies', () => {
            component.getCompanies();
            expect(component.companies).toEqual(testCompanies);
            expect(companiesService.getCompanies).toHaveBeenCalledTimes(1);
        });
    });
    describe('GetLocations', () => {
        it('should fetch the locations', () => {
            component.getLocations();
            expect(component.locations).toEqual(testLocations);
            expect(locationsService.getLocations).toHaveBeenCalledTimes(1);
        });
    });
    describe('AddEvent', () => {
        it('should post the event', () => {
            component.addEvent({ name: 'name', location_id: 1, company_id: 1 });
            expect(eventsService.addEvent).toHaveBeenCalledTimes(1);
            expect(eventsService.addEvent).toHaveBeenCalledWith({ name: 'name', location_id: 1, company_id: 1 });
        });
    });
    describe('OnEventClick', () => {
        it('should navigate to detail page', () => {
            component.onEventClick(15);
            expect(router.navigate).toHaveBeenCalledTimes(1);
            expect(router.navigate).toHaveBeenCalledWith(['/events', 15]);
        });
    });
    describe('OnPageClick', () => {
        it('should fetch the page data', () => {
            component.onPageClick(3);
            expect(eventsService.getEvents).toHaveBeenCalledTimes(1);
            expect(eventsService.getEvents).toHaveBeenCalledWith(3);
        });
    });
});
