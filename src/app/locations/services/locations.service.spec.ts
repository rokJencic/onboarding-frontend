import { testLocations } from '../../shared/_helpers/test.models';
import { Location } from '../../models/event.model';
import { LocationsService } from './locations.service';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

const locations: Location[] = testLocations;

describe('LocationsService', () => {
    let service: LocationsService;
    let httpClientSpy: {
        get: jasmine.Spy,
        post: jasmine.Spy,
        put: jasmine.Spy,
        delete: jasmine.Spy,
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [LocationsService],
        });
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
        service = new LocationsService(httpClientSpy as any);
    });
    describe('GetLocation', () => {
        it('should return the location', () => {
            httpClientSpy.get.and.returnValue(of(testLocations[0]));
            service.getLocation(1).subscribe(response => {
                expect(response).toEqual(testLocations[0]);
            });
        });
    });
    describe('getLocations', () => {
        it('should retrieve the locations', () => {
            httpClientSpy.get.and.returnValue(of(locations));
            service.getLocations().subscribe(response => {
                    expect(response).toEqual(locations);
                },
                fail
            );
            expect(httpClientSpy.get.calls.count()).toBe(1);
        });
    });

    describe('addLocation', () => {
        it('should post the location', () => {
            httpClientSpy.post.and.returnValue(of(locations[0]));
            service.addLocation(locations[0]).subscribe(response => {
                    expect(response).toEqual(locations[0]);
                },
                fail
            );
            expect(httpClientSpy.post.calls.count()).toBe(1);
        });
    });

    describe('updateLocation', () => {
        it('should update the location', () => {
            httpClientSpy.put.and.returnValue(of(locations[0]));
            service.updateLocation(locations[0], locations[0].id).subscribe(response => {
                    expect(response).toEqual(locations[0]);
                },
                fail
            );
            expect(httpClientSpy.put.calls.count()).toBe(1);
        });
    });

    describe('deleteLocation', () => {
        it('should delete the location', () => {
            httpClientSpy.delete.and.returnValue(of(locations[0].id));
            service.deleteLocation(locations[0].id).subscribe(response => {
                    expect(response).toEqual(locations[0].id);
                },
                fail
            );
            expect(httpClientSpy.delete.calls.count()).toBe(1);
        });
    });
});
