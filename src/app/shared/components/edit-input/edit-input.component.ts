import { Component , EventEmitter , Input , OnInit , Output } from '@angular/core';

export interface EmittedEvent {
    name: string;
    data: string | number;
}

@Component({
    selector: 'app-edit-input',
    templateUrl: './edit-input.component.html',
    styleUrls: ['./edit-input.component.scss'],
})
export class EditInputComponent implements OnInit {
    @Input() data: number | string;
    @Input() name: string;
    @Input() type: string;
    @Output() focusOut: EventEmitter<EmittedEvent> = new EventEmitter<EmittedEvent>();
    editMode = false;

    constructor() {}

    ngOnInit(): void {
    }

    onFocusOut() {
        this.focusOut.emit({ name: this.name, data: this.data });
        this.editMode = false;
    }
}
