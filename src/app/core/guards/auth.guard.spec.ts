import { AuthGuard } from './auth.guard';
import { User } from '../models/user.model';
import { BehaviorSubject } from 'rxjs';
import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot , Router , RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

const testUser: User = {
    id: 1,
    username: 'User 1',
    token: 'randomWebToken',
    admin: 1,
};

const userSubject = new BehaviorSubject<User>(testUser);

class MockService {
    currentUser = userSubject.asObservable();
    currentUserValue = testUser;
}

describe('AuthGuard', () => {
    let authGuard: AuthGuard;
    let router = {
        navigate: jasmine.createSpy('navigate'),
    };
    let state = {
        url: 'returnUrl',
    };
    let service: MockService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                AuthGuard,
                { provide: Router, useValue: router },
                { provide: RouterStateSnapshot, useValue: state },
                { provide: AuthenticationService, useClass: MockService },
            ],
        });
        authGuard = TestBed.get(AuthGuard);
        router = TestBed.get(Router);
        service = TestBed.get(AuthenticationService);
        state = TestBed.get(RouterStateSnapshot);
    });
    describe('CanActivate', () => {
        it('should return true on logged in user', () => {
            expect(authGuard.canActivate(new ActivatedRouteSnapshot(), state as RouterStateSnapshot)).toBeTruthy();
        });
        it('should navigate away on no logged in user', () => {
            service.currentUserValue = null;
            expect(authGuard.canActivate(new ActivatedRouteSnapshot(), state as RouterStateSnapshot)).toBeFalsy();
            expect(router.navigate).toHaveBeenCalledWith(['/login'], { queryParams: { returnUrl: 'returnUrl' }});
        });
    });
});
