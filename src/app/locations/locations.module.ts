import { NgModule } from '@angular/core';
import { LocationsComponent } from './containers/locations/locations.component';
import { LocationsRoutingModules } from './locations-routing.modules';
import { AddLocationComponent } from './components/add-location/add-location.component';
import { SingleLocationComponent } from './containers/single-location/single-location.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    declarations: [
        LocationsComponent,
        AddLocationComponent,
        SingleLocationComponent,
    ],
    imports: [
        LocationsRoutingModules,
        SharedModule,
    ]
})
export class LocationsModule {}
