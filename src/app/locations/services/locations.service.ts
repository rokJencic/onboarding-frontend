import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Location } from '../../models/event.model';

@Injectable({
    providedIn: 'root',
})
export class LocationsService {

    constructor(public http: HttpClient) {}

    getLocations(page: number = 1): Observable<any> {
        return this.http.get(`${environment.apiBaseUrl}/locations?page=${page}`);
    }

    addLocation(location: Location) {
        return this.http.post(`${environment.apiBaseUrl}/locations`, location);
    }

    getLocation(id: number): Observable<any> {
        return this.http.get(`${environment.apiBaseUrl}/locations/${id}`);
    }

    updateLocation(location: Location, id: number): Observable<any> {
        return this.http.put(`${environment.apiBaseUrl}/locations/${id}`, location);
    }

    deleteLocation(id: number): Observable<any> {
        return this.http.delete(`${environment.apiBaseUrl}/locations/${id}`);
    }
}
