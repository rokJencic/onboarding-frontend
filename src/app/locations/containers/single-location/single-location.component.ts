import { Component , OnInit } from '@angular/core';
import { LocationsService } from '../../services/locations.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '../../../models/event.model';

@Component({
    selector: 'app-single-location',
    templateUrl: './single-location.component.html',
    styleUrls: ['./single-location.component.scss'],
})
export class SingleLocationComponent implements OnInit {

    location: Location;
    unsavedChanges = false;

    constructor(
        private locationsService: LocationsService,
        private route: ActivatedRoute,
    ) {}

    ngOnInit(): void {
        this.loadLocation();
    }

    loadLocation() {
        const id = +this.route.snapshot.paramMap.get('id');
        this.locationsService.getLocation(id).subscribe(location => this.location = location[0]);
    }

    updateLocation(value) {
        this.location = {
            ...this.location,
            [value.name]: value.data,
        };
        this.unsavedChanges = true;
    }

    saveChanges() {
        this.locationsService.updateLocation(this.location, this.location.id).subscribe();
        this.unsavedChanges = false;
    }

    deleteLocation() {
        this.locationsService.deleteLocation(this.location.id).subscribe();
    }
}
