import { EditInputComponent } from './edit-input.component';
import { ComponentFixture , TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

describe('EditInputComponent', () => {
    let component: EditInputComponent;
    let fixture: ComponentFixture<EditInputComponent>;
    let el: DebugElement;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [EditInputComponent],
            imports: [FormsModule],
        });
        fixture = TestBed.createComponent(EditInputComponent);
        component = fixture.componentInstance;
        el = fixture.debugElement;
    });

    describe('Set Properties', () => {
        it('should set all input properties correctly', () => {
            component.data = 'String';
            expect(component.data).toEqual('String');

            component.name = 'Name';
            expect(component.name).toEqual('Name');

            component.type = 'Text';
            expect(component.type).toEqual('Text');

            expect(component.editMode).toBeFalsy();
        });
    });
    describe('onFocusOut', () => {
        it('should emit the correct event', () => {
            spyOn(component.focusOut, 'emit');

            component.name = 'Name';
            component.data = 'Data';

            component.onFocusOut();

            expect(component.focusOut.emit).toHaveBeenCalledTimes(1);
            expect(component.focusOut.emit).toHaveBeenCalledWith({ name: 'Name', data: 'Data' });
            expect(component.editMode).toBeFalsy();
        });
    });
    describe('ComponentTemplate', () => {
        it('should render the correctly', () => {
            component.name = 'Name';
            component.data = 'Data';
            fixture.detectChanges();
            const label = el.query(By.css('.form-group label'));
            const data = el.query(By.css('.form-group .form-control'));
            expect(label.nativeElement.textContent).toEqual('NAME: ');
            expect(data.nativeElement.textContent).toEqual(' Data ');
        });

        it('should render the input on editMode change', () => {
            component.name = 'name';
            component.data = 'Data';
            component.type = 'text';
            component.editMode = true;
            fixture.detectChanges();

            const label = el.query(By.css('.form-group label')).nativeElement;
            expect(label.textContent).toEqual('NAME: ');
            const input = el.query(By.css('.form-group input.form-control')).nativeElement;
            expect(input.type).toBe('text');
            expect(input.getAttribute('id')).toBe('name');
        });
    });
});
