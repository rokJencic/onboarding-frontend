import { LocationsComponent } from './locations.component';
import { ComponentFixture , TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { EMPTY , of } from 'rxjs';
import { testLocations } from '../../../shared/_helpers/test.models';
import { AddLocationComponent } from '../../components/add-location/add-location.component';
import { PaginationComponent } from '../../../shared/components/pagination/pagination.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LocationsService } from '../../services/locations.service';

describe('LocationsComponent', () => {
    let component: LocationsComponent;
    let fixture: ComponentFixture<LocationsComponent>;
    let router = {
        navigate: jasmine.createSpy('navigate'),
    };
    const locationsService = jasmine.createSpyObj('LocationsService', {
        getLocations: of({ data: { locations: testLocations }, meta: {}}),
        addLocation: EMPTY,
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                LocationsComponent,
                AddLocationComponent,
                PaginationComponent,
            ],
            providers: [
                { provide: Router, useValue: router },
                { provide: LocationsService, useValue: locationsService },
            ],
            imports: [
                ReactiveFormsModule,
                HttpClientTestingModule,
            ],
        });
        fixture = TestBed.createComponent(LocationsComponent);
        component = fixture.componentInstance;
        router = TestBed.get(Router);
        locationsService.getLocations.calls.reset();
    });
    describe('OnInit', () => {
        it('should fire correct methods', () => {
            component.ngOnInit();
            expect(locationsService.getLocations).toHaveBeenCalledTimes(1);
        });
    });
    describe('ChangeLocationsMode', () => {
        it('should change the mode', () => {
            expect(component.addLocationMode).toBeFalsy();
            component.changeMode();
            expect(component.addLocationMode).toBeTruthy();
            component.changeMode();
            expect(component.addLocationMode).toBeFalsy();
        });
    });
    describe('GetLocations', () => {
        it('should fetch the locations from service', () => {
            component.getLocations();
            expect(locationsService.getLocations).toHaveBeenCalledTimes(1);
            expect(component.locations).toEqual(testLocations);
        });
    });
    describe('AddLocation', () => {
        it('should post the location', () => {
            component.addLocation(testLocations[0]);
            expect(locationsService.addLocation).toHaveBeenCalledTimes(1);
            expect(locationsService.addLocation).toHaveBeenCalledWith(testLocations[0]);
        });
    });
    describe('OnPageClick', () => {
        it('should fetch new data', () => {
            component.onPageClick(5);
            expect(locationsService.getLocations).toHaveBeenCalledTimes(1);
            expect(locationsService.getLocations).toHaveBeenCalledWith(5);
        });
    });
    describe('OnLocationClick', () => {
        it('should navigate away', () => {
            component.onLocationClick(20);
            expect(router.navigate).toHaveBeenCalledTimes(1);
            expect(router.navigate).toHaveBeenCalledWith(['/locations', 20]);
        });
    });
});
