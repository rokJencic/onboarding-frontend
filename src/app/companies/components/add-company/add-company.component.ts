import { Component , EventEmitter , Input , OnInit , Output } from '@angular/core';
import { FormBuilder , FormGroup } from '@angular/forms';
import { AddCompany , Location } from '../../../models/event.model';

@Component({
    selector: 'app-add-company',
    templateUrl: './add-company.component.html',
    styleUrls: ['./add-company.component.scss']
})
export class AddCompanyComponent implements OnInit {
    addCompanyForm: FormGroup;
    @Input() locations: Location[];
    @Output() company = new EventEmitter<AddCompany>();

    constructor(private fb: FormBuilder) {}

    ngOnInit(): void {
        this.buildCompanyForm();
    }

    buildCompanyForm() {
        this.addCompanyForm = this.fb.group({
            name: this.fb.control([]),
            location_id: this.fb.control([]),
        });
    }

    submitCompanyForm() {
        this.company.emit(this.addCompanyForm.value);
    }
}
