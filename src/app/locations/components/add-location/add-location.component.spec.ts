import { AddLocationComponent } from './add-location.component';
import { ComponentFixture , TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormBuilder , ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

describe('AddLocationComponent', () => {
    let component: AddLocationComponent;
    let fixture: ComponentFixture<AddLocationComponent>;
    let el: DebugElement;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [AddLocationComponent],
            imports: [ReactiveFormsModule],
            providers: [FormBuilder],
        });
        fixture = TestBed.createComponent(AddLocationComponent);
        component = fixture.componentInstance;
        el = fixture.debugElement;
    });

    describe('BuildLocationForm', () => {
        it('should build the form', () => {
            component.buildLocationForm();
            fixture.detectChanges();

            const inputs = el.queryAll(By.css('.form-group'));
            expect(inputs.length).toEqual(3);
            const button = el.query(By.css('.form-group button'));
            expect(button.nativeElement.textContent).toBe('Add location');
        });
    });
    describe('SubmitLocationForm', () => {
        it('should emit correct event', () => {
            spyOn(component.location, 'emit');
            component.ngOnInit();
            component.addLocationForm.setValue({ name: 'Name', country: 'Country' });
            component.submitLocationForm();
            expect(component.location.emit).toHaveBeenCalledTimes(1);
            expect(component.location.emit).toHaveBeenCalledWith({ name: 'Name', country: 'Country' });
        });
    });
});
