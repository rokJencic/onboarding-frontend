import { HomepageComponent } from './homepage.component';
import { ComponentFixture , TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('HomepageComponent', () => {
    let fixture: ComponentFixture<HomepageComponent>;
    let el: DebugElement;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [HomepageComponent],
        });
        fixture = TestBed.createComponent(HomepageComponent);
        el = fixture.debugElement;
    });
    describe('CreateComponent', () => {
        it('should render the title in an h1 tag', () => {
            fixture.detectChanges();
            const title = el.query(By.css('h1'));
            expect(title.nativeElement.textContent).toContain('Welcome to Angular onboarding');
        });
    });
});
