import { Component , OnInit } from '@angular/core';
import { Company , Event , Location } from '../../../models/event.model';
import { EventsService } from '../../services/events.service';
import { ActivatedRoute } from '@angular/router';
import { LocationsService } from '../../../locations/services/locations.service';
import { CompaniesService } from '../../../companies/services/companies.service';

@Component({
    selector: 'app-single-event',
    templateUrl: './single-event.component.html',
    styleUrls: ['./single-event.component.scss'],
})
export class SingleEventComponent implements OnInit {
    event: Event;
    locations: Location[];
    companies: Company[];

    constructor(
        private eventsService: EventsService,
        private locationsService: LocationsService,
        private companiesService: CompaniesService,
        private route: ActivatedRoute,
    ) {}

    ngOnInit(): void {
        this.loadEvent();
        this.loadCompanies();
        this.loadLocations();
    }

    loadEvent() {
        const id = +this.route.snapshot.paramMap.get('id');
        this.eventsService.getEvent(id).subscribe(event => this.event = event);
    }

    loadCompanies() {
        this.companiesService.getCompanies().subscribe(companies => this.companies = companies.data.companies);
    }

    loadLocations() {
        this.locationsService.getLocations().subscribe(locations => this.locations = locations.data.locations);
    }

    updateEvent(value) {
        this.event = {
            ...this.event,
            [value.name]: value.data,
        };
    }

    updateCompany(company) {
        this.event.company = company;
    }

    updateLocation(location) {
        this.event.location = location;
    }

    saveChanges() {
        this.eventsService.updateEvent(this.event.id,
            { name: this.event.name, company_id: this.event.company.id, location_id: this.event.location.id })
            .subscribe();
    }

    deleteEvent() {
        this.eventsService.deleteEvent(this.event.id).subscribe();
    }
}
