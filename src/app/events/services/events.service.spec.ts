import { testEvents } from '../../shared/_helpers/test.models';
import { EventsService } from './events.service';
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { Event } from '../../models/event.model';

const events: Event[] = testEvents;

describe('EventsService', () => {
    let service: EventsService;
    let httpClientSpy: {
        get: jasmine.Spy,
        post: jasmine.Spy,
        put: jasmine.Spy,
        delete: jasmine.Spy,
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [EventsService],
        });
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
        service = new EventsService(httpClientSpy as any);
    });
    describe('GetEvent', () => {
        it('should return the event', () => {
            httpClientSpy.get.and.returnValue(of(testEvents[0]));
            service.getEvent(1).subscribe(response => {
                expect(response).toEqual(testEvents[0]);
            });
        });
    });
    describe('getEvents', () => {
        it('should retrieve the events', () => {
            httpClientSpy.get.and.returnValue(of(events));
            service.getEvents().subscribe(response => {
                    expect(response).toEqual(events);
                },
                fail
            );
            expect(httpClientSpy.get.calls.count()).toBe(1);
        });
    });

    describe('addEvent', () => {
        it('should post the event', () => {
            httpClientSpy.post.and.returnValue(of(events[0]));
            service.addEvent({ name: 'Test Event', location_id: 1, company_id: 1}).subscribe(response => {
                    expect(response).toEqual(events[0]);
                },
                fail
            );
            expect(httpClientSpy.post.calls.count()).toBe(1);
        });
    });

    describe('updateEvent', () => {
        it('should update the event', () => {
            httpClientSpy.put.and.returnValue(of(events[0]));
            service.updateEvent(events[0].id, { name: 'Test Company', location_id: 1, company_id: 1 }).subscribe(response => {
                    expect(response).toEqual(events[0]);
                },
                fail
            );
            expect(httpClientSpy.put.calls.count()).toBe(1);
        });
    });

    describe('deleteEvent', () => {
        it('should delete the event', () => {
            httpClientSpy.delete.and.returnValue(of(events[0].id));
            service.deleteEvent(events[0].id).subscribe(response => {
                    expect(response).toEqual(events[0].id);
                },
                fail
            );
            expect(httpClientSpy.delete.calls.count()).toBe(1);
        });
    });
});
