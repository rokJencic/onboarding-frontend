import { Component , OnInit } from '@angular/core';
import { LocationsService } from '../../services/locations.service';
import { Location } from '../../../models/event.model';
import { Router } from '@angular/router';

@Component({
    selector: 'app-locations',
    templateUrl: './locations.component.html',
    styleUrls: ['./locations.component.scss'],
})
export class LocationsComponent implements OnInit {
    locations: Location[];
    addLocationMode = false;
    currentPage: number;
    lastPage: number;

    constructor(
        private locationsService: LocationsService,
        private router: Router,
    ) {}

    ngOnInit(): void {
        this.getLocations();
    }

    changeMode() {
        this.addLocationMode = !this.addLocationMode;
    }

    getLocations() {
        this.locationsService.getLocations().subscribe(response => {
            this.locations = response.data.locations;
            this.currentPage = response.meta.current_page;
            this.lastPage = response.meta.last_page;
        });
    }

    addLocation(location: Location) {
        this.locationsService.addLocation(location).subscribe();
    }

    onLocationClick(id: number) {
        this.router.navigate(['/locations', id]);
    }

    onPageClick(page: number) {
        this.locationsService.getLocations(page).subscribe(response => {
            this.locations = response.data.locations;
            this.currentPage = response.meta.current_page;
        });
    }
}
