import { RouterModule , Routes } from '@angular/router';
import { CompaniesComponent } from './containers/companies/companies.component';
import { NgModule } from '@angular/core';
import { SingleCompanyComponent } from './containers/single-company/single-company.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: CompaniesComponent
    },
    {
        path: ':id',
        component: SingleCompanyComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CompaniesRoutingModule {}
