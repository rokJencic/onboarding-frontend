import { Component , EventEmitter , Input , Output } from '@angular/core';

@Component({
    selector: 'app-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent {
    @Input() currentPage: number;
    @Input() lastPage: number;
    @Output() pageClick = new EventEmitter<number>();

    constructor() {}

    onPageClick(page: number) {
        this.pageClick.emit(page);
    }

    createRange() {
        let start = 1;
        let range = this.lastPage;
        if (this.currentPage - 3 > 0) {
            start = this.currentPage - 3;
        }
        if (this.currentPage + 3 <= this.lastPage) {
            range = this.currentPage + 3;
        }
        const items: number[] = [];
        for (let i = start; i <= range; i++) {
            items.push(i);
        }
        return items;
    }
}
