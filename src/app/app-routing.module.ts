import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/guards/auth.guard';


const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./core/core.module').then(c => c.CoreModule),
  },
  { path: 'events',
    loadChildren: () => import('./events/events.module').then(e => e.EventsModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'companies',
    loadChildren: () => import('./companies/companies.module').then(c => c.CompaniesModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'locations',
    loadChildren: () => import('./locations/locations.module').then(l => l.LocationsModule),
    canActivate: [AuthGuard],
  },
  {
    path: '**', redirectTo: '/',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
