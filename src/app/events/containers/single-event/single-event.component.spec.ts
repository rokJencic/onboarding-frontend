import { SingleEventComponent } from './single-event.component';
import { ComponentFixture , TestBed } from '@angular/core/testing';
import { testCompanies , testEvents , testLocations } from '../../../shared/_helpers/test.models';
import { EMPTY , of } from 'rxjs';
import { EditSelectComponent } from '../../../shared/components/edit-select/edit-select.component';
import { EditInputComponent } from '../../../shared/components/edit-input/edit-input.component';
import { EventsService } from '../../services/events.service';
import { CompaniesService } from '../../../companies/services/companies.service';
import { LocationsService } from '../../../locations/services/locations.service';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';

describe('SingleEventComponent', () => {
    let component: SingleEventComponent;
    let fixture: ComponentFixture<SingleEventComponent>;
    let route = {
        snapshot: {
            paramMap: {
                get: (key: string) => testEvents[0].id,
            },
        },
    };
    const eventsService = jasmine.createSpyObj('EventsService', {
        getEvent: of(testEvents[0]),
        updateEvent: EMPTY,
        deleteEvent: EMPTY,
    });
    const companiesService = jasmine.createSpyObj('CompaniesService', {
        getCompanies: of({ data: { companies: testCompanies }}),
    });
    const locationsService = jasmine.createSpyObj('LocationsService', {
        getLocations: of({ data: { locations: testLocations }}),
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                SingleEventComponent,
                EditSelectComponent,
                EditInputComponent,
            ],
            providers: [
                { provide: EventsService, useValue: eventsService },
                { provide: CompaniesService, useValue: companiesService },
                { provide: LocationsService, useValue: locationsService },
                { provide: ActivatedRoute, useValue: route },
            ],
            imports: [
                RouterTestingModule,
                FormsModule,
            ],
        });
        fixture = TestBed.createComponent(SingleEventComponent);
        component = fixture.componentInstance;
        route = TestBed.get(ActivatedRoute);
        eventsService.getEvent.calls.reset();
        companiesService.getCompanies.calls.reset();
        locationsService.getLocations.calls.reset();
    });
    describe('OnInit', () => {
        it('should fire correct methods', () => {
            component.ngOnInit();
            expect(eventsService.getEvent).toHaveBeenCalledTimes(1);
            expect(companiesService.getCompanies).toHaveBeenCalledTimes(1);
            expect(locationsService.getLocations).toHaveBeenCalledTimes(1);
        });
    });
    describe('LoadEvent', () => {
        it('should fetch the event', () => {
            component.loadEvent();
            expect(eventsService.getEvent).toHaveBeenCalledTimes(1);
            expect(eventsService.getEvent).toHaveBeenCalledWith(1);
            expect(component.event).toEqual(testEvents[0]);
        });
    });
    describe('LoadCompanies', () => {
        it('should load the companies', () => {
            component.loadCompanies();
            expect(companiesService.getCompanies).toHaveBeenCalledTimes(1);
            expect(component.companies).toEqual(testCompanies);
        });
    });
    describe('LoadLocations', () => {
        it('should load the locations', () => {
            component.loadLocations();
            expect(locationsService.getLocations).toHaveBeenCalledTimes(1);
            expect(component.locations).toEqual(testLocations);
        });
    });
    describe('UpdateEvent', () => {
        it('should update the event', () => {
            component.event = testEvents[0];
            component.updateEvent({ name: 'name', data: 'New Name' });
            component.updateEvent({ name: 'company', data: testCompanies[1] });
            component.updateEvent({ name: 'location', data: testLocations[1] });
            expect(component.event).toEqual({
                ...testEvents[0],
                name: 'New Name',
                company: testCompanies[1],
                location: testLocations[1],
            });
        });
    });
    describe('UpdateCompany', () => {
        it('should update the company', () => {
            component.event = testEvents[0];
            expect(component.event.company).toEqual(testCompanies[0]);
            component.updateCompany(testCompanies[1]);
            expect(component.event.company).toEqual(testCompanies[1]);
        });
    });
    describe('UpdateLocation', () => {
        it('should update the location', () => {
            component.event = testEvents[0];
            expect(component.event.location).toEqual(testLocations[0]);
            component.updateLocation(testLocations[1]);
            expect(component.event.location).toEqual(testLocations[1]);
        });
    });
    describe('SaveChanges', () => {
        it('should save the changes', () => {
            component.event = testEvents[0];
            const newEvent = { ...testEvents[0], name: 'New Name', company: testCompanies[1], location: testLocations[1] };
            component.event = newEvent;
            component.saveChanges();
            expect(eventsService.updateEvent).toHaveBeenCalledTimes(1);
            expect(eventsService.updateEvent).toHaveBeenCalledWith(
                testEvents[0].id, {
                    name: 'New Name',
                    company_id: testCompanies[1].id,
                    location_id: testLocations[1].id,
                });
        });
    });
    describe('DeleteEvent', () => {
        it('should delete the event', () => {
            component.event = testEvents[0];
            component.deleteEvent();
            expect(eventsService.deleteEvent).toHaveBeenCalledTimes(1);
            expect(eventsService.deleteEvent).toHaveBeenCalledWith(testEvents[0].id);
        });
    });
});
