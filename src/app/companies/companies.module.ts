import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CompaniesComponent } from './containers/companies/companies.component';
import { CompaniesRoutingModule } from './companies-routing.module';
import { SharedModule } from '../shared/shared.module';
import { SingleCompanyComponent } from './containers/single-company/single-company.component';
import { AddCompanyComponent } from './components/add-company/add-company.component';

@NgModule({
    declarations: [
        CompaniesComponent,
        SingleCompanyComponent,
        AddCompanyComponent,
    ],
    imports: [
        HttpClientModule,
        SharedModule,
        CompaniesRoutingModule,
    ],
    providers: [],
})
export class CompaniesModule {}
