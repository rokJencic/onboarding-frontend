import { NgModule } from '@angular/core';
import { EventsComponent } from './containers/events/events.component';
import { EventsRoutingModule } from './events-routing.modules';
import { SharedModule } from '../shared/shared.module';
import { SingleEventComponent } from './containers/single-event/single-event.component';
import { AddEventComponent } from './components/add-event/add-event.component';

@NgModule({
    declarations: [
        EventsComponent,
        SingleEventComponent,
        AddEventComponent,
    ],
    imports: [
        EventsRoutingModule,
        SharedModule,
    ],
    providers: [],
})
export class EventsModule {}
