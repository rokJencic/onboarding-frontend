import { Component , OnInit } from '@angular/core';
import { EventsService } from '../../services/events.service';
import { AddEvent , Company , Event , Location } from '../../../models/event.model';
import { CompaniesService } from '../../../companies/services/companies.service';
import { LocationsService } from '../../../locations/services/locations.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-events',
    templateUrl: './events.component.html',
    styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
    events: Event[];
    companies: Company[];
    locations: Location[];
    addEventMode = false;
    currentPage: number;
    lastPage: number;

    constructor(
        private eventsService: EventsService,
        private companiesService: CompaniesService,
        private locationsService: LocationsService,
        private router: Router,
    ) {}

    ngOnInit(): void {
        this.getEvents();
        this.getCompanies();
        this.getLocations();
    }

    changeMode() {
        this.addEventMode = !this.addEventMode;
    }

    getEvents() {
        this.eventsService.getEvents().subscribe(response => {
            this.events = response.data.events;
            this.currentPage = response.meta.current_page;
            this.lastPage = response.meta.last_page;
        });
    }

    getCompanies() {
        this.companiesService.getCompanies().subscribe(companies => this.companies = companies.data.companies);
    }

    getLocations() {
        this.locationsService.getLocations().subscribe(locations => this.locations = locations.data.locations);
    }

    addEvent(event: AddEvent) {
        this.eventsService.addEvent(event).subscribe();
    }

    onEventClick(id: number) {
        this.router.navigate(['/events', id]);
    }

    onPageClick(page: number) {
        this.eventsService.getEvents(page).subscribe(response => {
           this.events = response.data.events;
           this.currentPage = response.meta.current_page;
        });
    }
}
