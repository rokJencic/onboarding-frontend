
import { ComponentFixture , TestBed } from '@angular/core/testing';
import { testCompanies , testLocations } from '../../../shared/_helpers/test.models';
import { EMPTY , of } from 'rxjs';
import { SingleCompanyComponent } from './single-company.component';
import { EditInputComponent } from '../../../shared/components/edit-input/edit-input.component';
import { EditSelectComponent } from '../../../shared/components/edit-select/edit-select.component';
import { CompaniesService } from '../../services/companies.service';
import { ActivatedRoute } from '@angular/router';
import { LocationsService } from '../../../locations/services/locations.service';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';

describe('SingleCompanyComponent', () => {
    let component: SingleCompanyComponent;
    let fixture: ComponentFixture<SingleCompanyComponent>;
    let route = {
        snapshot: {
            paramMap: {
                get: (key: string) => testCompanies[0].id,
            },
        },
    };
    const companiesService = jasmine.createSpyObj('CompaniesService', {
        getCompany: of(testCompanies[0]),
        updateCompany: EMPTY,
        deleteCompany: EMPTY,
    });
    const locationsService = jasmine.createSpyObj('LocationsService', {
        getLocations: of({ data: { locations: testLocations }}),
    });

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                SingleCompanyComponent,
                EditInputComponent,
                EditSelectComponent,
            ],
            providers: [
                { provide: CompaniesService, useValue: companiesService },
                { provide: ActivatedRoute, useValue: route },
                { provide: LocationsService, useValue: locationsService },
            ],
            imports: [
                RouterTestingModule,
                FormsModule,
            ],
        });
        fixture = TestBed.createComponent(SingleCompanyComponent);
        component = fixture.componentInstance;
        route = TestBed.get(ActivatedRoute);
        companiesService.getCompany.calls.reset();
        locationsService.getLocations.calls.reset();
    });
    describe('OnInit', () => {
        it('should fire correct methods', () => {
            component.ngOnInit();
            expect(locationsService.getLocations).toHaveBeenCalledTimes(1);
            expect(companiesService.getCompany).toHaveBeenCalledTimes(1);
        });
    });
    describe('LoadCompany', () => {
        it('should fetch the company', () => {
            component.loadCompany();
            expect(companiesService.getCompany).toHaveBeenCalledTimes(1);
            expect(companiesService.getCompany).toHaveBeenCalledWith(1);
            expect(component.company).toEqual(testCompanies[0]);
        });
    });
    describe('LoadLocations', () => {
        it('should fetch the locations', () => {
            component.loadLocations();
            expect(locationsService.getLocations).toHaveBeenCalledTimes(1);
            expect(component.locations).toEqual(testLocations);
        });
    });
    describe('UpdateCompany', () => {
        it('should update the company', () => {
            component.company = testCompanies[0];
            component.updateCompany({ name: 'name', data: 'New Name' });
            component.updateCompany({ name: 'location', data: testLocations[1] });
            expect(component.company).toEqual({ ...testCompanies[0], name: 'New Name', location: testLocations[1] });
        });
    });
    describe('UpdateLocation', () => {
        it('should update the location', () => {
            component.company = testCompanies[0];
            component.updateLocation(testLocations[1]);
            expect(component.company.location).toEqual(testLocations[1]);
        });
    });
    describe('SaveChanges', () => {
        it('should fire the request', () => {
            component.company = testCompanies[0];
            const newCompany = { ...testCompanies[0], name: 'New Name', location: testLocations[1] };
            component.company = newCompany;
            component.saveChanges();
            expect(companiesService.updateCompany).toHaveBeenCalledTimes(1);
            expect(companiesService.updateCompany).toHaveBeenCalledWith(
                testCompanies[0].id,
                { name: newCompany.name, location_id: newCompany.location.id });
        });
    });
    describe('DeleteCompany', () => {
        it('should delete the company', () => {
            component.company = testCompanies[0];
            component.deleteCompany();
            expect(companiesService.deleteCompany).toHaveBeenCalledTimes(1);
            expect(companiesService.deleteCompany).toHaveBeenCalledWith(testCompanies[0].id);
        });
    });
});
