import { RouterModule , Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LocationsComponent } from './containers/locations/locations.component';
import { SingleLocationComponent } from './containers/single-location/single-location.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: LocationsComponent
    },
    {
        path: ':id',
        component: SingleLocationComponent,
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LocationsRoutingModules {}
