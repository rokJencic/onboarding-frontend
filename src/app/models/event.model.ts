export interface Event {
    id: number;
    name: string;
    company: Company;
    location: Location;
}

export interface Company {
    id: number;
    name: string;
    location: Location;
}

export interface Location {
    id: number;
    name: string;
    country: string;
}

export interface AddCompany {
    name: string;
    location_id: number;
}

export interface AddEvent {
    name: string;
    company_id: number;
    location_id: number;
}
