import { Component , EventEmitter , Input , OnInit , Output } from '@angular/core';

export interface Option {
    id: number;
    name: string;
}

@Component({
    selector: 'app-edit-select',
    templateUrl: './edit-select.component.html',
    styleUrls: ['./edit-select.component.scss'],
})
export class EditSelectComponent implements OnInit {
    @Input() data: Option;
    @Input() name: string;
    private privateOptions: Option[];
    @Output() focusOut: EventEmitter<Option> = new EventEmitter<Option>();
    editMode = false;
    selectedOption: Option;
    @Input() set options(options: Option[]) {
        this.privateOptions = options;
        this.selectedOption = this.options[this.data.id - 1];
    }
    get options() {
        return this.privateOptions;
    }

    constructor() {}

    ngOnInit(): void {
    }

    onFocusOut() {
        this.focusOut.emit(this.selectedOption);
        this.editMode = false;
    }
}
